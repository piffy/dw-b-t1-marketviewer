package src;
import java.io.*;
import java.awt.*;
/**
 * This class receives a DataDealer object from the constructor from which it derives the information that will be inserted into the HTML file by the htmlBuild method.
 */
public class HtmlWork {
    String html;
    DataDealer dd;
    File tabella = new File("src/html/index.html");
    Calc math;
    public boolean isOpened=false;
    public HtmlWork(DataDealer dd) {
        this.dd=dd;
        math=new Calc(dd);
    }
    /**
     * This method uses an already written html file and inserts the information contained in the datadealer into it.
     * @throws IOException
     */
    public void htmlBuild() throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(tabella));
        bw.write("<!DOCTYPE html>\n" +
                "<html lang=\"en\" dir=\"ltr\">\n<meta http-equiv=\"refresh\" content=\"25\" >" +
                "  <head>\n" +
                "    <meta charset=\"utf-8\">\n" +
                "    <title>MARKETVIEWER</title>\n" +
                "    <link href=\"style.css\" type=\"text/css\" rel=\"stylesheet\">\n" +
                "    <script type=\"text/javascript \" src=\"https://code.jquery.com/jquery-3.6.0.min.js\"></script>\n" +
                "    <script src=\"https://cdn.jsdelivr.net/npm/chart.js@3.7.0/dist/chart.min.js\"></script>\n" +
                "    <link href=\"https://fonts.googleapis.com/css2?family=Alfa+Slab+One&display=swap\" rel=\"stylesheet\">\n" +
                "<!--    <script src=\"javascript.js\"></script> -->\n" +
                "  </head>\n" +
                "  <body>\n" +
                "\n" +
                "    <div id=\"title\">\n" +
                "      <img id=\"logo\" src=\"logo.png\">\n" +
                "      <span id=\"mv\">MARKETVIEWER</span>\n" +
                "    </div>\n" +
                "    <div id=\"table\">\n" +
                "      <table align=\"center\">\n" +
                "        <tr>\n" +
                "          <th style=\"width:40%\">Info</th>\n" +
                "          <th style=\"width:60%\">Description</th>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "          <th>Trading Symbol: "+dd.getTicker()+"<br>        Company: "+dd.getSociety()+"<br>        Address: "+dd.getStockAddress()+"<br>        City/State: "+dd.getSocInfo()+"<br>    </th>\n" +
                "          <th>"+dd.getDescription()+"</th>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "          <th colspan=\"2\">Period of time: "+dd.getStartingDate()+" / "+dd.getCloseDate()+"<br>    The open price for the symbol in the given time period: "+dd.getOpen()+"<br>    The close price for the symbol in the given time period: "+dd.getClose()+"<br>    The highest price for the symbol in the given time period: "+dd.getHigh()+"<br>    The lowest price for the symbol in the given time period: "+dd.getLow()+"<br>    Theoretical maximum achievable (using one stock): "+math.maxProfit()+"<br>\n" +
                "Difference between the opening and closing value in the given period (using one stock ): "+math.periodGap()+"<br>    Percentage of days growing over the period: "+math.dayOfGrowth()+"<br></th>\n" +
                "        </tr>\n" +
                "      </table>\n" +
                "    </div>\n" +
                "\n" +
                "    <div id=\"graphs\">\n" +
                "      <div id=\"graph1\"><canvas id=\"chart1\" width=\"100\" height=\"45\"></canvas>\n" +
                "      <script>\n" +
                "      const ctx = document.getElementById('chart1')\n" +
                "      closes = [];\n" +
                "      opens = [];\n" +
                "      highs = [];\n" +
                "      lows = [];\n" +
                "      labels = [];\n" +
                "      max = 0;\n" +
                "      min = 0;\n" +
                "      const giorni = ["+math.totalDays()+"];\n" +
                "      const myChart = new Chart(ctx, {\n" +
                "              type: 'line',\n" +
                "              data: {\n" +
                "                  labels: giorni,\n" +
                "                  datasets: [{\n" +
                "                      label: \"Closed Market Value\",\n" +
                "                      data: "+dd.getAllCloseVal()+",\n" +
                "                      fill: false,\n" +
                "                      borderColor: 'rgb(75, 192, 192)',\n" +
                "                      tension: 0.1\n" +
                "                  }, {\n" +
                "                      label: \"Opened Market Value\",\n" +
                "                      data: "+dd.getAllOpenVal()+",\n" +
                "                      fill: false,\n" +
                "                      borderColor: 'rgb(95, 70, 192)',\n" +
                "                      tension: 0.1\n" +
                "                  }, ]\n" +
                "              },\n" +
                "              options: {\n" +
                "                  scales: {\n" +
                "                      x: {\n" +
                "                          ticks: {\n" +
                "                              color: 'whitesmoke'\n" +
                "                          }\n" +
                "                      },\n" +
                "                      y: {\n" +
                "                          ticks: {\n" +
                "                              color: 'whitesmoke'\n" +
                "                          }\n" +
                "                      }\n" +
                "                  },\n" +
                "                  plugins: {\n" +
                "                      legend: {\n" +
                "                          labels: {\n" +
                "                              color: 'whitesmoke'\n" +
                "                          }\n" +
                "                      }\n" +
                "                  }\n" +
                "              }\n" +
                "          })\n" +
                "      </script></div>\n" +
                "      <div id=\"graph2\"><canvas id=\"chart2\" width=\"100\" height=\"45\"></canvas>\n" +
                "      <script>\n" +
                "      const stx = document.getElementById('chart2')\n" +
                "      closes = [];\n" +
                "      opens = [];\n" +
                "      highs = [];\n" +
                "      lows = [];\n" +
                "      labels = [];\n" +
                "      max = 0;\n" +
                "      min = 0;\n" +
                "      const myS = new Chart(stx, {\n" +
                "              type: 'line',\n" +
                "              data: {\n" +
                "                  labels: giorni,\n" +
                "                  datasets: [{\n" +
                "                      label: \"Closed Market Value\",\n" +
                "                      data: "+dd.getAllCloseVal()+",\n" +
                "                      fill: false,\n" +
                "                      borderColor: 'rgb(75, 192, 192)',\n" +
                "                      tension: 0.1\n" +
                "                  }, {\n" +
                "                      label: \"Weighted Moving Average\",\n" +
                "                      data: "+math.weightedMovingAvarage()+",\n" +
                "                      fill: false,\n" +
                "                      borderColor: 'rgb(00, 00, 255)',\n" +
                "                      tension: 0.1\n" +
                "                  },{\n" +
                "                      label: \"Simple Moving Average\",\n" +
                "                      data: "+math.simpleMovingAvarage()+",\n" +
                "                      fill: false,\n" +
                "                      borderColor: 'rgb(95, 70, 192)',\n" +
                "                      tension: 0.1\n" +
                "                  }, ]\n" +
                "              },\n" +
                "              options: {\n" +
                "                  scales: {\n" +
                "                      x: {\n" +
                "                          ticks: {\n" +
                "                              color: 'whitesmoke'\n" +
                "                          }\n" +
                "                      },\n" +
                "                      y: {\n" +
                "                          ticks: {\n" +
                "                              color: 'whitesmoke'\n" +
                "                          }\n" +
                "                      }\n" +
                "                  },\n" +
                "                  plugins: {\n" +
                "                      legend: {\n" +
                "                          labels: {\n" +
                "                              color: 'whitesmoke'\n" +
                "                          }\n" +
                "                      }\n" +
                "                  }\n" +
                "              }\n" +
                "          })\n" +
                "      </script></div>\n" +
                "    </div>\n" +
                "  </body>\n" +
                "</html>\n");
        bw.close();
    }
    /**
     * opens the html file
     * @throws IOException
     */
    public void htmlOpen() throws IOException {
        isOpened=true;
        Desktop.getDesktop().open(new File("src/html/index.html"));
    }
}