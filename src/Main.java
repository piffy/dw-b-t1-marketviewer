package src;

import java.io.IOException;

public class Main {
    /**
     * in the Main class we make all calls to all classes
     */
    public static void main(String[] args) throws IOException, InterruptedException {
        Url url = new Url("https://dwweb.gnet.it/dw2022/");
        DataDealer dd = new DataDealer(url);
        HtmlWork htmlWork = new HtmlWork(dd);
        htmlWork.htmlBuild();
        htmlWork.htmlOpen();
        while (true){
            Thread.sleep(30000);
            url=new Url("https://dwweb.gnet.it/dw2022/");
            dd=new DataDealer(url);
            htmlWork=new HtmlWork(dd);
            htmlWork.htmlBuild();
        }
    }
}

