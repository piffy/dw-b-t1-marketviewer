09/02/2022
DISCUSSIONE INTERNA

1. Produttività (es: siamo lenti?)
2. Processo (es: scadenze in ritardo?)
3. Risultati (es: il codice è scadente?)
4. Team (es: litighiamo troppo?)
5. Strumenti di lavoro (es: lo strumento X non è adatto?)
6. Ambiente di lavoro (es: c’è troppa confusione?)

1. per quanto riguarda la produttivitá siamo abbastanza in linea 
con quanto ci aspettavamo, peró c'é ampio margine di miglioramento.
2. per il momento non é ancora stato riscontrato nessun ritardo
nelle consegne.
3. non del tutto, ma anche qui ci sono molte cose che vanno migliorate,
per esempio javadoc, l'uml (che va ristrutturato) e in generale la
sintassi del codice.
4. non abbiamo mai avuto problemi interni di comunicazione e in generale
la situazione nel team é abbastanza pacifica e vivibile, l'unica cosa 
é che a volte potrebbero capitare incomprensioni che vanno a incidere
sul lavoro svolto sui singoli membri.
5. all ok
6. solo piccoli problemi di concentrazione.

POSSIBILI RISOLUZIONI

1. per migliorare la produttivitá dobbiamo lavorare in maniera piú 
omogenea, per esempio facendo almeno una user story a testa al giorno.
2. dobbiamo continuare con questo ritmo per poter finire tutto in tempo.
3. per migliorare la qualitá del codice, e quella della documentazione,
abbiamo pensato di istituire un Reviewer di tutto ció che viene fatto
in uno sprint, il cui ruolo é quello di controllare che tutto sia fatto 
bene, e che rispetti i nostri standard. (Romano Riccardo)
4. l'unico modo per migliorare la comunicazione é quello di confrontarsi
piú spesso e piú chiaramente.
5. okok
6. abbiamo deciso che tra un'ora e l'altra abbiamo 5 minuti di pausa in 
modo tale che durante l'ora complessiva ci sia un lavoro continuo e una
maggiore concentrazione.

10/02/2022
Risoluzione dei problemi dell'ultima volta:
1. la produttività è stata sicuramente migliorata sia dalle idee che sono state 
pensate durante l'ultima discussione e anche grazie ai consigli che ci sono arrivati dai prof
durante il meet di mercoledì.
2. il ritmo procede in maniera decente e direi che siamo a buon punto.
3. il ruolo di Reviewer assegnato a Romano Riccardo si è rivelato utile per migliorare la qualità del
codice prodotto.
4. la comunicazione è migliorata leggermente ma c'è ancora margine di miglioramento.
6. sicuramente il punto che ha riscosso più risultati, considerando che la produttività di un individuo dipende anche
dalla sua sanità mentale, direi che 5 minuti di pausa dopo un'ora di programmazzione sono più che giusti, oltre al fatto
che sprona il lavoratore a impegnarsi di più durante il momento di lavoro, e a volte anche quando sarebbe ora di prendersi
una pausa esso non si ferma, per finire prima quello che stava facendo.

Problemi 
1. Un problema del gruppo è la differenza di abilità tra i membri, per esempio un individuo è magari più bravo
in una cosa e un altro in un altra, questo crea spesso dei problemi di comunicazione che generano incomprensioni
che riflettono i loro effetti negativi sul prodotto finito.
2. Ci sono stati problemi riguardanti la comprensione delle richieste nella documentazione, questi hanno ovviamente avuto un 
impatto negativo su tutto il progetto.

Possibili Risoluzioni 
1. Per ovviare a questo problema bisogna che tutti i membri non si concentrino solo sullo scrivere il codice ma anche
che si accertino che anche gli altri membri capiscano cosa sia stato fatto e qual è l'obbiettivo di quell'azione.
Infatti d'ora in poi prima di committare bisognerà prima spiegare agli altri i cambiamenti fatti, in modo tale che 
non si ritrovino a pullare un codice diverso da come se lo ricordavano.
2. Per fortuna questi problemi ci sono stati segnalati nella videochiamata di mercoledì, ora che abbiamo riletto la documentazione
abbiamo capito cosa avevamo sbagliato e abbiamo aggiustato il codice secondo le indicazioni dei prof.
