Sprint 07/02/22
US completed:
1- As a developer i want to be able to have the latest data of the stock- 5
2- As a developer i want to be able to easly access the data- 2
3- As a developer i want to be able to write the html file- 2
4- As a developer i want to be able to have the html file with the stock data- 4

Sprint 08/02/2022
US completed:
6- As a user i want to be able to see the stock name in the html- 1
8- As a user i want to be able to see the the stock's HQ address- 3
9- As a user i want to be able to see the agency's description- 3
10- As a user i want to be able to see the updated stock's data- 4

Sprint 09/02/2022
US completed:
11- As a user i want to be able to see the stock's data in a determined period of time- 4  *
12- As a user i want to be able to see the highest market value in a determined period of time- 2 *
13- As a user i want to be able to see the lowest market value in a determined period of time- 2 *
14- As a user i want to be able to see the closed market value in a determined period of time- 2 *
15- As a user i want to be able to see the opened market value in a determined period of time- 2 *
16- As a user i want to be able to see the theorical max profit in a determined period of time- 2 *

Sprint 10/02/2022
US completed:
18- As a user i want to be able to have the data updated every 30sec- 3 *
19- As a developer i want to be able to have the meta-tag for the auto-refresh in the header of the html *
+ restructuring of the code and improvement of javadoc


