# T1 Sprint 2 checklist

* Documentazione di progettazione aggiornata: PARTIAL (UML sgrammaticato, migliorabile, discrepanze con sorgente) - NESSUNA MODIFICA DALLO SPRINT PRECEDENTE; 
* Sprint backlog aggiornato su trello: OK (qualche confusione tra backlog e sprint backlog, nonché tra US e issues)
* Codice : YES
* Codice JUnit : NO
* JavaDoc: OK - UNCOMPILED
* Company logo: YES
* Documentazione in formato gittabile (grafici esclusi) : YES (bonus)
* Diario giornaliero delle attività personali (incluse ore fuori orario) : PARTIAL - NOT UPDATED (bonus)
* Daily status: YES 
* Review video: OK
* Retrospective : OK

