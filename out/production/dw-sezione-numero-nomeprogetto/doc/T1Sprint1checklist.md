# T1 Sprint 1 checklist

* Documentazione di progettazione aggiornata: PARTIAL (UML sgrammaticato, migliorabile, discrepanze con sorgente); 
* Sprint backlog aggiornato su trello: OK (un problema sugli Sprint backlog)
* Codice : YES
* Codice JUnit : NO
* JavaDoc: INSUFFICIENT
* Documentazione in formato gittabile (grafici esclusi) : NO (bonus)
* Diario giornaliero delle attività personali (incluse ore fuori orario) : YES (bonus)
* Daily status: YES 
* Review video: YES (some problems)
* Retrospettiva : YES


