package src;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import  java.util.*;



public class Url {
    /**
     * This class has the role of making a rest call to the requested link in the documentation,
     * once the call is made it divides the data and uses an api to find the information on the requested stock,
     * within the required time frame. Once the call to the api is made, the information is returned as a string.
     */
    public String[] restInfo;
    public String stock, startDate, closeDate;
    public String urlData;
    public String urlTickerData;

    /**
     * The constructor takes care of receiving the link to which to make the rest call and building the link for the Polygon Api, with the information received.
     * @param url
     */
    public Url(String url){
        HttpRequest request = HttpRequest.newBuilder().uri(URI.create(url)).method("GET", HttpRequest.BodyPublishers.noBody()).build();
        HttpResponse<String> response = null;
        try {
            response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        restInfo=response.body().split(",");
        stock=restInfo[0].split(":")[1];
        startDate=restInfo[1].split(":")[1];
        closeDate=restInfo[2].split(":")[1];
        stock=stock.substring(stock.indexOf("\"")+1,stock.lastIndexOf("\""));
        startDate=startDate.substring(startDate.indexOf("\"")+1,startDate.lastIndexOf("\""));
        closeDate=closeDate.substring(closeDate.indexOf("\"")+1,closeDate.lastIndexOf("\""));
        startDate=startDate.replaceAll("/", "-");
        closeDate=closeDate.replaceAll("/", "-");
        urlData="https://api.polygon.io/v2/aggs/ticker/"+stock+"/range/1/day/"+startDate.substring(6,10)+startDate.substring(2,6)+startDate.substring(0,2)+"/"+closeDate.substring(6,10)+closeDate.substring(2,6)+closeDate.substring(0,2)+"?adjusted=true&sort=asc&limit=120&apiKey=OZMucyo4dhla8798iLatAxkaREzVFkE8";
        urlTickerData="https://api.polygon.io/v3/reference/tickers/"+stock+"?apiKey=OZMucyo4dhla8798iLatAxkaREzVFkE8";
    }
    /**
     * Once the link for polygon is built, this method takes care of making the call and returning the values obtained as a String.
     * this call retrieves market data such as: market value at close or market value at opening.
     */
    public String downloadData() throws IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder().uri(URI.create(urlData)).method("GET", HttpRequest.BodyPublishers.noBody()).build();
        HttpResponse<String> response;
        response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        return response.body();
    }
    /**
     * Once the link for polygon is built, this method takes care of making the call and returning the values obtained as a String.
     * this call retrieves information about the company such as: the address, description and full name of the company.
     */
    public String downloadTickerData() throws IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder().uri(URI.create(urlTickerData)).method("GET", HttpRequest.BodyPublishers.noBody()).build();
        HttpResponse<String> response;
        response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        return response.body();
    }


    /**
     * the dataExtractor method allows you to take the raw data of downloadData and break it into an array.
     * it returns the clean array
     */
    public String[] extract(String data) {
        String arrayData[] = new String[999];
        for (int i = 0; i < arrayData.length; i++) {
            arrayData = data.split("\":");
        }
        return arrayData;
    }
}

//Url{data='{"ticker":"AAPL","queryCount":1,"resultsCount":1,"adjusted":true,"results":[{"v":7.7287356e+07,"vw":146.991,"o":145.935,"c":146.8,"h":148.195,"l":145.81,"t":1626926400000,"n":480209}],"status":"OK","request_id":"1f3e9cf7c3ccb7b7bccf45a154279292","count":1}'}