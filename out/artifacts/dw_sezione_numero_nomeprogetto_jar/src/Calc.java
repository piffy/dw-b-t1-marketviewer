package src;

import java.util.ArrayList;

public class Calc {

    /**
     * The Calc class allows you to perform mathematical calculations essential to the success of the program.
     */
    private DataDealer dd;

    public Calc(DataDealer dataDealer){
        this.dd = dataDealer;
    }
    /**
     * This method allows you to calculate the theoretical maximum achievable if you always bought at the minimum and always sold at the maximum on each day.
     */
    public double maxProfit(){
        double maxValue=0;
        for (int i=0; i<dd.getAllHighVal().size(); i++){
            maxValue+=dd.getAllHighVal().get(i)-dd.getAllLowVal().get(i);
        }
        maxValue=(double) (Math.round(maxValue*100.0)/100.0);
        return  maxValue;
    }

    /**
     * This method returns a string of numbers representing the total days, which is necessary for the graphs.
     * @return days
     */
    public String totalDays(){
        String days="";
        for (int i=1; i<=dd.getAllOpenVal().size(); i++){
            days+="\""+i+"\",";
        }
        return days;
    }

    /**
     * This method allows you to calcualte the percentage of days in which the stock has grown during the requested period
     * @return growth %
     */
    public String dayOfGrowth(){
        int growthCount=0;
        for (int i=0; i<dd.getAllOpenVal().size(); i++){
            if (dd.getAllOpenVal().get(i)>dd.getAllCloseVal().get(i)){
                growthCount++;
            }
        }
        return ((growthCount*100)/dd.getAllCloseVal().size())+"%";
    }
    /**
     * This method allows you to calculate how much the stock has gained (or lost) for the required period
     * @return gap
     */
    public String periodGap(){
        double gap=0;
        gap=dd.getClose()-dd.getOpen();
        gap=(Math.round(gap*100.0)/100.0);
        return " "+gap;
    }
    public String simpleMovingAvarage(){
        ArrayList<Double> sma = new ArrayList<Double>();
        double tot;
        int j;
        for (int i=1; i<=dd.getAllCloseVal().size(); i++){
            tot=0.0;
            for (j=0; j<i; j++){
                tot+=dd.getAllCloseVal().get(j);
            }
            tot=tot/i;
            sma.add(tot);
        }
        return sma.toString();
    }
    public String weightedMovingAvarage() {
        ArrayList<Double> wma = new ArrayList<Double>();
        double tot;
        int j, div=0;
        for (int i=1; i<=dd.getAllCloseVal().size(); i++){
            tot=0.0;
            div=0;
            for (j=0; j<i; j++){
                tot+=dd.getAllCloseVal().get(j)*(j+1);
                div+=j+1;
            }
            tot=tot/div;
            wma.add(tot);
        }
        return wma.toString();
    }
}
