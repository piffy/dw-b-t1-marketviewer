package src;

import java.io.IOException;
import java.util.ArrayList;

public class DataDealer {
    /**
     * the DataDealer class takes care of receiving a url object in the constructor and through the functions of the latter downloads the information into an array,
     * which is then processed and the individual information is divided into variables accessible through getters.
     */
    Url url;
    String[] arrayData=new String[4324];
    String data;
    private String description;
    private String ticker, stockAddress, stockDescription="", society="", socInfo;
    private Double low=9999999.9, high=0.0, open=0.0, close=0.0;
    private ArrayList<Double> allCloseVal= new ArrayList<Double>();
    private ArrayList<Double> allOpenVal= new ArrayList<Double>();
    private ArrayList<Double> allHighVal= new ArrayList<Double>();
    private ArrayList<Double> allLowVal= new ArrayList<Double>();

    /**
     * the constructor of this class receives an Url object and extracts its data first into a generic array,
     * then into individual variables and 4 ArrayList that contain the numerical data of each day.
     * @param url
     * @throws IOException
     * @throws InterruptedException
     */
    public DataDealer(Url url) throws IOException, InterruptedException {
        this.url = url;
        data = url.downloadData();
        data += url.downloadTickerData();
        arrayData = url.extract(data);
        ticker=url.stock;
        for (int j = 0; j < arrayData.length; j++) {
            if (arrayData[j].contains("description") && stockDescription.isEmpty()) {
                stockDescription = arrayData[j + 1];
            }
            if (arrayData[j].contains("city")) {
                socInfo = arrayData[j + 1].split(",")[0] + " " + arrayData[j + 2].split(",")[0] + ", " + arrayData[j + 3].split("}")[0];
            }
            if (arrayData[j].contains("name") && society.isBlank()) {
                society = arrayData[j + 1].split(",")[0];
            }
            if (arrayData[j].contains("address1")) {
                stockAddress = arrayData[j + 1].split(",")[0];
            }
            if (arrayData[j].contains("\"o")) {
                if (open==0){
                    open = Double.parseDouble(arrayData[j + 1].split(",")[0]);
                }
                close = Double.parseDouble(arrayData[j + 2].split(",")[0]);
                allCloseVal.add(close);
                allOpenVal.add(Double.parseDouble(arrayData[j + 1].split(",")[0]));
                allHighVal.add(Double.parseDouble(arrayData[j + 3].split(",")[0]));
                allLowVal.add(Double.parseDouble(arrayData[j + 4].split(",")[0]));
                if (Double.parseDouble(arrayData[j + 3].split(",")[0])>high){
                    high = Double.parseDouble(arrayData[j + 3].split(",")[0]);
                }
                if (Double.parseDouble(arrayData[j + 4].split(",")[0])<low){
                    low = Double.parseDouble(arrayData[j + 4].split(",")[0]);
                }
            }
            if (arrayData[j].contains("\"description")){
                description=arrayData[j+1].split(",\"sic")[0];
            }
        }
        System.out.println(data);
    }

    /**
     * these are all the getters whose task of course is to give access to all the variables of the class, the return values can be both numerical and textual.
     */
    public String getDescription() {
        return description;
    }

    public String getTicker() {
        return ticker;
    }

    public String getStockAddress() {
        return stockAddress;
    }

    public String getStockDescription() {
        return stockDescription;
    }

    public String getSociety() {
        return society;
    }

    public String getSocInfo() {
        return socInfo;
    }

    public Double getLow() {
        return low;
    }

    public Double getHigh() {
        return high;
    }

    public Double getOpen() {
        return open;
    }

    public Double getClose() {
        return close;
    }
    public ArrayList<Double> getAllCloseVal(){
        return allCloseVal;
    }

    public ArrayList<Double> getAllOpenVal() {
        return allOpenVal;
    }

    public ArrayList<Double> getAllHighVal() {
        return allHighVal;
    }

    public ArrayList<Double> getAllLowVal() {
        return allLowVal;
    }
    public String getStartingDate(){
        return url.startDate;
    }
    public String getCloseDate(){
        return url.closeDate;
    }
}

