document.onload = loading();
function loading(){
  const giorni = ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22",];
  new Chart("chart1",
  {
    type: "line",
    data: {
      labels: giorni,
      datasets: [
        {
          label: "Opened Market Value",
          fill: false,
          lineTension: 0,
          borderColor: "#d0d0d0",
          data: [167.48, 158.735, 164.02, 164.29, 169.08, 172.125, 174.91, 175.205, 181.115, 175.25, 175.11, 179.28, 169.93, 168.28, 171.555, 173.04, 175.85, 177.085, 180.16, 179.33, 179.47, 178.085],
          tension: 0
        },
        {
          label: "Closed Market Value",
          fill: false,
          lineTension: 0,
          borderColor: "#FFFF00",
          data: [164.77, 163.76, 161.84, 165.32, 171.18, 175.08, 174.56, 179.45, 175.74, 174.33, 179.3, 172.26, 171.14, 169.75, 172.99, 175.64, 176.28, 180.33, 179.29, 179.38, 178.2, 177.57],
          tension: 0
        }
      ]
    },
    options: {
      legend: {
        display: false
      },
      color: "#ffffff",
      scales: {
        yAxes: [
          {
            ticks: {
              fontColor: "white",
            }
          }
        ],
        xAxes: [
          {
            ticks: {
              fontColor: "white",
            }
          }
        ]
      }
    }
  });
}
