User stories totali (fatte) 13/13
Commento US: Generalmente buone. Manca qualsiasi riferimento al fatto che l'input derivi dalla chiamate rest al server; di fatto le US non sono mai state aggiornate. User è troppo generico: "investor", "speculator", "curios", "Statistic maniac". Esplicitare il motivo sarebbe servito.
Stimate: si, Prioritizzate: si
Organizzazione in sprint (velocity): si -  13/17/6 (media per giorno 7, stabile)

CRITICA
5- As a user I want to be able to open the html file from a .jar- 1 *
	THIS IS TECHNICAL AND BADLY WRITTEN
6- As a user I want to be able to see the stock name in the html- 1  *
	OK
7- As a user I want to be able to see the stock exchange square- 3
	OK
8- As a user I want to be able to see the the stock's HQ address- 3  *
	OK
9- As a user I want to be able to see the agency's description- 3  *
	OK
10- As a user I want to be able to see the updated stock's data- 4  *
	UNCLEAR. WHAT DOES IT MEAN? "MORE RECENT QUOTE" OR "NEW STOCK FROM THE PROVIDER"
11- As a user I want to be able to see the stock's data in a determined period of time- 4  *
	STOCK DATA CAN BE BROKEN DOWN IN SEVERAL SUBSTORIES
12- As a user I want to be able to see the highest market value in a determined period of time- 2 *
	OK
13- As a user I want to be able to see the lowest market value in a determined period of time- 2 *
	OK
14- As a user I want to be able to see the closed market value in a determined period of time- 2 *
	OK
15- As a user I want to be able to see the opened market value in a determined period of time- 2 *
	OK
16- As a user I want to be able to see the theoretical max profit in a determined period of time- 2 *
	OK
17- As a user I want to be able to see the two graphs- 5 *
	WHICH GRAPHS?
18- As a user I want to be able to have the data updated every 30sec- 3 *
	OK
	
	
