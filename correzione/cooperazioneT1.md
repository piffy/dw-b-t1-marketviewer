# TEAMWORK

## Impegno
Ruoli ufficiali Non esplicitati
Dedotti dalle relazioni: 
Romano - coordinatore/scrum master
Fontana, Haladiy - HTML/CSS
Belletti, Franciosi - Codifica backend
 
 Ore lavorate
Romano R. 23+38
Franciosi M. 20+35
Belletti M.22+37
Haladiy A. 20+25
Fontana L.  26+25




## Git 
Franciosi 59 (DataDealer.java,HtmlWork.java,Url.java,Main.java, index.html, User_Stories.txt, Retrospective.txt, Sprint.txt)
Romano 32 (HtmlWork.java, javascript.js, Main.Java, DataDealer.java, index.html, User_Stories.txt, etrospective.txt, Sprint.txt)
Belletti 24 (Retrospective.txt, Sprint.txt, User_Stories.txt, index.html)
Fontana 2 (ProjectDescription.txt)
Haladiy 0

Web upload 12/137

Nota: Haladiy e Fontana devono migliorare il loro rapporto con git. Il loro contributo figura SOLO tramite le relazioni. 


## Retrospective
Bene eseguita e applicata


## Sintesi
L'immagine che emerge è quella di un gruppo che si è ben organizzato ed ha cercato, e in gran parte è riuscito, a sfruttare le perculiarità individuali, bilanciando abbastanza bene l'impegno individuale. Lo sviluppo pare essere stato portato avanti spesso in coppia, con frequenti interazioni con il gruppo, sfruttando abbastanza bene le tecnologie proposte. Lo stress pare limitato. 



S


